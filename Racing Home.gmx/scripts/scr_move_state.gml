/// scr_move_state()

if (GameInput.ACTION_KEY) {
    base_state = scr_interact_state;
    instance_create(x, y-25, PlayerInteractAnimation);
    scr_create_interact_box();
}

image_speed = .3;

// Get direction
dir = point_direction(0, 0, GameInput.xaxis, GameInput.yaxis);

// Get the length
if (GameInput.xaxis == 0 and GameInput.yaxis == 0) {
    len = 0;    
} else {
    len = spd;
    // get face direction when we moving
    scr_get_face_dir();
}

// Get the hspd and vspd
hspd = lengthdir_x(len, dir);
vspd = lengthdir_y(len, dir)

// Move the player
phy_position_x += hspd;
phy_position_y += vspd;

// Control the sprite
//image_speed = sign(len)*0.3;
if (len == 0) image_index = 0;

// face dir is getting while moving
switch (face_dir) {
    case RIGHT_DIR:
        sprite_index = spr_player_right;
        break;
        
    case LEFT_DIR:
        sprite_index = spr_player_left;
        break;
        
    case UP_DIR:
        sprite_index = spr_player_up;
        break;
        
    case DOWN_DIR:
        sprite_index = spr_player_down;
        break;
}
                        
