/// scr_npc_slime_interact_state()

// Working with interaction_timer
if (interaction_timer >= 0) {
    interaction_timer -= (1 / room_speed); 
} else {
    interaction_timer = DEFAULT_INTERACTION_TIMER;
    scr_set_npc_speed(self, DEFAULT_SPD, DEFAULT_IMG_SPD);
    base_state = scr_npc_slime_idle_state;
}

