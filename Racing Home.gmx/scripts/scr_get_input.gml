/// scr_get_input()

RIGHT_KEY = keyboard_check(ord('D'));
LEFT_KEY = keyboard_check(ord('A'));
DOWN_KEY = keyboard_check(ord('S'));
UP_KEY = keyboard_check(ord('W'));
ACTION_KEY = keyboard_check_pressed(ord('J'));
INVENTORY_KEY = keyboard_check_pressed(ord('K'));

NO_INPUT = keyboard_check(vk_nokey);

// Get the axis
xaxis = (RIGHT_KEY - LEFT_KEY);
yaxis = (DOWN_KEY - UP_KEY);

// Check for gamepad input
var gp_id = 0;
var threshold = .5;

if (gamepad_is_connected(gp_id)) {
    gamepad_set_axis_deadzone(gp_id, .35);
    xaxis = gamepad_axis_value(gp_id, gp_axislh);
    yaxis = gamepad_axis_value(gp_id, gp_axislv);
    
    ACTION_KEY = gamepad_button_check_pressed(gp_id, gp_face1);
    INVENTORY_KEY = gamepad_button_check_pressed(gp_id, gp_face3);
    NO_INPUT = keyboard_check(vk_nokey);    
}

