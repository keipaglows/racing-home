/// scr_create_interact_box()

var ib_x = 0;
var ib_y = 0;

switch (sprite_index) {
    case spr_player_right:
        ib_x = x + 30;
        ib_y = y + 14;
        break;
        
    case spr_player_left:
        ib_x = x - 30;
        ib_y = y + 14;
        break;
        
    case spr_player_up:
        ib_x = x;
        ib_y = y - 10;
        break;
        
    case spr_player_down:
        ib_x = x;
        ib_y = y + 40;
        break;
}

instance_create(ib_x, ib_y, PlayerInteractAction);

