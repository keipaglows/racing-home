/// scr_npc_slime_idle_state()

var is_turned_int = (is_turned * 2 - 1)

// Working with timer
if (turn_timer >= 0) {
    phy_position_x += is_turned_int * spd;
    turn_timer -= (1 / room_speed);
} else {
    is_turned = !is_turned;
    turn_timer = DEFAULT_TURN_TIMER;
}

image_xscale = is_turned_int;

